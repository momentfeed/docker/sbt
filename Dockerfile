# This Dockerfile has two required ARGs to determine which base image
# to use for the JDK and which sbt version to install.

ARG OPENJDK_TAG=8u342
FROM openjdk:${OPENJDK_TAG}

ARG SBT_VERSION=1.8.3

# prevent this error: java.lang.IllegalStateException: cannot run sbt from root directory without -Dsbt.rootdir=true; see sbt/sbt#1458
WORKDIR /app

# Install sbt
RUN \
  mkdir /working/ && \
  cd /working/ && \
  curl -L -o sbt-$SBT_VERSION.deb https://repo.scala-sbt.org/scalasbt/debian/sbt-$SBT_VERSION.deb && \
  dpkg -i sbt-$SBT_VERSION.deb && \
  rm sbt-$SBT_VERSION.deb && \
  curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -; \
  echo "deb [arch=amd64] https://download.docker.com/linux/debian buster stable" | tee -a /etc/apt/sources.list.d/docker.list; \
  apt-get update && \
  apt-get install -y sbt docker-ce-cli && \
  rm -rf /var/lib/apt/lists/* && \
  cd && \
  rm -r /working/ && \
  touch a.scala && \
  sbt 'set crossScalaVersions := Seq("2.10.7", "2.11.12", "2.12.17", "2.12.18", "2.13.10", "2.13.11", "3.2.2", "3.3.0")' '+compile' && \
  rm a.scala && \
  sbt clean sbtVersion

